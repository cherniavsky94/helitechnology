'use strict';

/*
    This file can be used as entry point for webpack!
 */

import jquery from 'jquery';

import ScrollMagic from 'scrollmagic';

import { TweenMax, TimelineMax, Linear } from 'gsap';

import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';


global.$ = global.jQuery = jquery;

(function (sections) {

  if (sections.length === 0)
    return;

  const controller = new ScrollMagic.Controller();


  const pageHeader = document.querySelector('.js-page-header');
  const pageHeaderHeight = parseFloat(getComputedStyle(pageHeader).height);

  let firstSection = null;

  sections.forEach((section, index) => {
    let isFirst = (index === 0);
    let isEven = (index % 2 !== 0);

    let height = parseFloat(getComputedStyle(section).height);

    let content = section.querySelector('.js-main-section-content');
    let overlay = section.querySelector('.js-main-section-overlay');

    let options = {
      scene: {
        content: {
          fadeIn: {
            triggerElement: section,
            offset: -(height / 2),
            duration: height / 2 - pageHeaderHeight,
            triggerHook: 'onLeave'
          },

          fadeOut: {
            triggerElement: section,
            offset: isFirst ? 0 : -pageHeaderHeight,
            duration: height / 2 - pageHeaderHeight,
            triggerHook: 'onLeave'
          }
        },

        overlay: {
          triggerElement: section,
          offset: -pageHeaderHeight,
          duration: isEven ? height - pageHeaderHeight : height + pageHeaderHeight,
          triggerHook: 'onLeave'
        },
      },

      tweenMax: {
        content: {
          fadeIn: {
            autoAlpha: 1,
            ease: Linear.easeNone
          },
          fadeOut: {
            autoAlpha: 0,
            ease: Linear.easeNone
          }
        },

        overlay: {
          rotation: isEven ? 0 : 90,
          ease: Linear.easeNone
        }
      }
    };

    if (isFirst || isEven) {
      options.scene.overlay.offset = 0;
    }

    if (isFirst) {
      firstSection = section;
      options.scene.overlay.duration = height;
    }

    let sceneOverlay = new ScrollMagic.Scene(options.scene.overlay);

    if (overlay) {
      sceneOverlay
        .setTween(TweenMax.to(overlay, 1, options.tweenMax.overlay))
        .addTo(controller)
        .on('end', (event) => {
          section = sections[index + 2];

          if (section) {
            if (event.scrollDirection === 'FORWARD') {
              section.classList.add('is-visible');
            } else if (event.scrollDirection === 'REVERSE') {
              section.classList.remove('is-visible');
            }
          }
        });
    }

    if (content) {
      if (isFirst === false) {
        TweenMax.to(content, 1, {
          autoAlpha: 0
        });
      }

      let sceneContentFadeOut = new ScrollMagic.Scene(options.scene.content.fadeOut);

      sceneContentFadeOut
        .setTween(TweenMax.to(content, 1, options.tweenMax.content.fadeOut))
        .addTo(controller);


      let sceneContentFadeIn = new ScrollMagic.Scene(options.scene.content.fadeIn);

      sceneContentFadeIn
        .setTween(TweenMax.to(content, 1, options.tweenMax.content.fadeIn))
        .addTo(controller);;
    }

  });



  let firstSectionHeight = parseFloat(getComputedStyle(firstSection).height);


  new ScrollMagic.Scene({
    offset: pageHeaderHeight,
    duration: pageHeaderHeight,
    triggerHook: 0
  }).setTween(TweenMax.to(pageHeader, 1, {
    y: '-100%',
    autoAlpha: 0,
    ease: Linear.easeNone
  })).addTo(controller);

  new ScrollMagic.Scene({
    offset: firstSectionHeight - pageHeaderHeight * 2,
    duration: pageHeaderHeight,
    triggerHook: 0
  }).setTween(TweenMax.to(pageHeader, 1, {
    y: '0%',
    autoAlpha: 1,
    ease: Linear.easeNone
  }))
    .addTo(controller)
    .on('progress', function (event) {
      let hasClassFixed = pageHeader.classList.contains('is-fixed');

      if (event.progress === 0 && hasClassFixed) {
        pageHeader.classList.remove('is-fixed');
      } else {
        pageHeader.classList.add('is-fixed');
      }
    });

})(document.querySelectorAll('.js-main-section'));
