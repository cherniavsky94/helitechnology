var $document = $(document);
var $window = $(window);
var $body = $('body');


window.APP = window.APP || {};

APP.Plugins = APP.Plugins || {};
APP.Components = APP.Components || {};
APP.Helpers = APP.Helpers || {};

APP.Preloader = {
  timer: undefined,

  init: function() {
    var component = this;
    var $preloaderPercent = $('.js-preloader-percent');
    var value = 0;

    component.timer = setInterval(function () {
      if (value <= 100) {
        $preloaderPercent.html(value++);
      } else {
        component.end();
      }
    }, 30);
  },

  end: function () {
    clearInterval(this.timer);
    $body.removeClass('page--preloader');
  }
};


(function($, APP) {
  APP.Initilizer = function() {
    var app = {};

    app.init = function() {
      app.initGlobalPlugins();
      app.initPlugins();
      app.initComponents();
    };

    app.onLoadTrigger = function() {
      // APP.Plugins.Preloader.loaded();
      // APP.Plugins.LazyLoadImages.init();
    };

    app.refresh = function() {
      app.initPlugins(true);
      app.initComponents(true);
    };

    app.destroy = function() {};

    // pjax triggers
    app.newPageReady = function() {
      app.refresh();
    };

    app.transitionCompleted = function() {
      APP.Plugins.AOS.refresh();
      app.onLoadTrigger();
    };

    // Global plugins which must be initialized once
    app.initGlobalPlugins = function() {
      // APP.Plugins.Barba.init();
    };

    // Plugins which depends on DOM and page content
    app.initPlugins = function(fromPjax) {
      // APP.Plugins.Fullpage.init();
    };

    // All components from `src/componenets`
    app.initComponents = function(fromPjax) {
      // APP.Preloader.init();
    };

    return app;
  };

  // a.k.a. ready
  $(function() {
    APP.Initilizer().init();
  });

  $window.on('load', function() {
    $.ready.then(function() {
      APP.Initilizer().onLoadTrigger();
    });
  });
})(jQuery, APP);
