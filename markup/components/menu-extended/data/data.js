var data = {menuExtended: [
  {
    href: '#',
    title: 'Продажа вертолётов',
    submenu: [
      {
        href: '#',
        title: 'Airbus Helicopters'
      },
      {
        href: '#',
        title: 'Bell Helicopters'
      },
      {
        href: '#',
        title: 'Leonardo Helicopters'
      },
      {
        href: '#',
        title: 'Российские вертолеты'
      }
    ]
  },
  {
    href: '#',
    title: 'О компании',
    submenu: [
      {
        href: '#',
        title: 'Пресс-релизы'
      },
      {
        href: '#',
        title: 'Контактная информация'
      }
    ]
  },
  {
    href: '#',
    title: 'Владельцам вертолётов',
    submenu: [
      {
        href: '#',
        title: 'Продать свой вертолет'
      },
      {
        href: '#',
        title: 'Коммерческое обеспечение'
      }
    ]
  },
  {
    href: '#',
    title: 'Вертоводство'
  },
  {
    href: '#',
    title: 'Финансирование'
  },
  {
    href: '#',
    title: 'Вертолётные работы'
  }
]};