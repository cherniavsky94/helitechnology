var data = {sectionProducts: {
  new: [
    {
      href: '#',
      photo: {
        src: '__static__img/content/products/1.jpg'
      },
      title: 'EUROCOPTER EC130',
      properties: [
        {
          label: 'Налет часов:',
          value: '820 часов'
        },
        {
          label: 'Год выпуска:',
          value: '2009 г'
        },
        {
          label: 'Собственник:',
          value: 'Юр. лицо'
        },
        {
          label: 'Местонахождение:',
          value: 'Москва, Россия'
        }
      ],
      price: {
        label: 'Стоимость:',
        value: '4 850 000 €'
      }
    },
    {
      href: '#',
      photo: {
        src: '__static__img/content/products/2.jpg'
      },
      badge: {
        type: 'primary',
        title: 'Новый'
      },
      title: 'EUROCOPTER EC130',
      properties: [
        {
          label: 'Налет часов:',
          value: '820 часов'
        },
        {
          label: 'Год выпуска:',
          value: '2009 г'
        },
        {
          label: 'Собственник:',
          value: 'Юр. лицо'
        },
        {
          label: 'Местонахождение:',
          value: 'Москва, Россия'
        }
      ],
      price: {
        label: 'Стоимость:',
        value: '4 850 000 €'
      }
    },
    {
      href: '#',
      photo: {
        src: '__static__img/content/products/3.jpg'
      },
      badge: {
        type: 'white',
        title: 'Продан'
      },
      title: 'EUROCOPTER EC130',
      properties: [
        {
          label: 'Налет часов:',
          value: '820 часов'
        },
        {
          label: 'Год выпуска:',
          value: '2009 г'
        },
        {
          label: 'Собственник:',
          value: 'Юр. лицо'
        },
        {
          label: 'Местонахождение:',
          value: 'Москва, Россия'
        }
      ],
      price: {
        label: 'Стоимость:',
        value: '4 850 000 €'
      }
    }
  ]
}};