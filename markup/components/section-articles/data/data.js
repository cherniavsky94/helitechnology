var data = {sectionArticles: [
    {
      href: '#',
      badge: {
        title: 'Статья Forbes',
        type: 'dark'
      },
      date: '21.06.2019',
      title: 'Как выбрать вертолёт?',
      caption: 'Личная встреча поможет определить потребности и точно сформировать предложение на конкретный борт. Обычно, первая встреча займет не более 10 мин вашего времени.',
      photo: {
        src: '__static__img/content/articles/1.jpg'
      },
      logo: '__static__img/content/forbes-logo.png',
      more: {
        title: 'Читать полностью'
      }
    },
    {
      href: '#',
      badge: {
        title: 'Статья РБК',
        type: 'dark'
      },
      date: '21.06.2019',
      title: 'На какие моменты обратить внимание при осмотре вертолёта?',
      caption: 'Личная встреча поможет определить потребности и точно сформировать предложение на конкретный борт. Обычно, первая встреча займет не более 10 мин вашего времени.',
      photo: {
        src: '__static__img/content/articles/2.jpg'
      },
      logo: '__static__img/content/rbk-logo.png',
      more: {
        title: 'Читать полностью'
      }
    },
    {
      href: '#',
      badge: {
        title: 'Статья РБК',
        type: 'dark'
      },
      date: '21.06.2019',
      title: 'Стоимость содержания личного вертолета',
      caption: 'Личная встреча поможет определить потребности и точно сформировать предложение на конкретный борт. Обычно, первая встреча займет не более 10 мин вашего времени.',
      photo: {
        src: '__static__img/content/articles/3.jpg'
      },
      logo: '__static__img/content/village-logo.png',
      more: {
        title: 'Читать полностью'
      }
    },
    {
      href: '#',
      badge: {
        title: 'Статья газета.ру',
        type: 'dark'
      },
      date: '21.06.2019',
      title: 'Когда стоит задуматься о покупке вертолета',
      caption: 'Личная встреча поможет определить потребности и точно сформировать предложение на конкретный борт. Обычно, первая встреча займет не более 10 мин вашего времени.',
      photo: {
        src: '__static__img/content/articles/4.jpg'
      },
      logo: '__static__img/content/gazeta-ru-logo.png',
      more: {
        title: 'Читать полностью'
      }
    },
    {
      href: '#',
      badge: {
        title: 'Статья Village',
        type: 'dark'
      },
      date: '21.06.2019',
      title: 'Какие риски ждут клиента при покупке ресурсного вертолета',
      caption: 'Личная встреча поможет определить потребности и точно сформировать предложение на конкретный борт. Обычно, первая встреча займет не более 10 мин вашего времени.',
      photo: {
        src: '__static__img/content/articles/5.jpg'
      },
      logo: '__static__img/content/village-logo.png',
      more: {
        title: 'Читать полностью'
      }
    }
]};
